## Movie Catalogue App

### Description:
This is a React application that displays movie information using cards. Each card contains details such as movie title, languages, countries, and genres. It also includes a "See More" button to expand the content if it exceeds two lines.

The movie data is filtered and processed to remove duplicates before being displayed in the cards. This ensures that each option in the languages, countries, and genres sections is unique.

### Install dependencies and run the application:
npm install
npm run start


### Technologies Used:
- React
- Tailwind CSS

### Repository Link:
[Movie Catalogue App Repository](https://gitlab.com/gzlfathima786/movie-catalogue.git)
