import React, { useState } from "react";
import SearchableDropdown from "./SearchableDropdown";

const Filter = ({
  languageOptions,
  genreOption,
  countryOption,
  filteredMovies,
  setFilteredMovies,
  movies,
}) => {
  const [selectedLanguage, setSelectedLanguage] = useState("");
  const [selectedGenre, setSelectedGenre] = useState("");
  const [selectedCountry, setSelectedCountry] = useState("");

  const handleLanguageChange = (selectedLanguage) => {
    setSelectedLanguage(selectedLanguage);
    filterMovies(selectedLanguage, selectedGenre, selectedCountry);
  };

  const handleGenreChange = (selectedGenre) => {
    setSelectedGenre(selectedGenre);
    filterMovies(selectedLanguage, selectedGenre, selectedCountry);
  };

  const handleCountryChange = (selectedCountry) => {
    setSelectedCountry(selectedCountry);
    filterMovies(selectedLanguage, selectedGenre, selectedCountry);
  };

  const filterMovies = (language, genre, country) => {
    // Filter movies based on the selected language, genre, and country
    const filtered = movies.filter((movie) => {
      const languageMatch =
        !language || movie.movielanguages.includes(language);
      const genreMatch = !genre || movie.moviegenres.includes(genre);
      const countryMatch = !country || movie.moviecountries.includes(country);
      return languageMatch && genreMatch && countryMatch;
    });
    setFilteredMovies(filtered);
  };

  // console.log(filteredMovies, "filteredMovies");
  return (
    <div className="flex flex-wrap gap-6 items-center justify-center">
      <SearchableDropdown
        value={selectedLanguage}
        label="Language:"
        placeholder="Select Language"
        options={languageOptions}
        onChange={handleLanguageChange}
        removeOption={() => {
          setSelectedLanguage("");
          filterMovies("", selectedGenre, selectedCountry);
        }}
        smallText={true}
        width={"w-[150px]"}
        downIcon
        searchPlaceHolder="Search Language"
      />
      <SearchableDropdown
        value={selectedGenre}
        label="Genre:"
        placeholder="Select Genre"
        options={genreOption}
        onChange={handleGenreChange}
        removeOption={() => {
          setSelectedGenre("");
          filterMovies(selectedLanguage, "", selectedCountry);
        }}
        smallText={true}
        width={"w-[150px]"}
        downIcon
        searchPlaceHolder="Search Genre"
      />
      <SearchableDropdown
        value={selectedCountry}
        label="Country:"
        placeholder="Select Country"
        options={countryOption}
        onChange={handleCountryChange}
        removeOption={() => {
          setSelectedCountry("");
          filterMovies(selectedLanguage, selectedGenre, "");
        }}
        smallText={true}
        width={"w-[150px]"}
        downIcon
        searchPlaceHolder="Search Country"
      />
    </div>
  );
};

export default Filter;
