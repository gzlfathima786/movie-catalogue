import React, { useEffect, useMemo, useRef, useState } from "react";
import { motion } from "framer-motion";
import HipchatChevronDownIcon from "../assets/HipchatChevronDownIcon";
import useOnClickOutside from "../hook/useClickOutside";
import ErrorMessage from "../components/ErrorText";
import CrossIcon from "../assets/CrossIcon";

const SearchableDropdown = ({
  options,
  value,
  label,
  leftText = false,
  onChange,
  placeholder = "Select an option",
  disabled = false,
  width,
  downIcon = false,
  searchPlaceHolder,
  valueFont = "text-[14px] font-gilroy-regular",
  totalPage,
  page,
  setPage,
  hint,
  externalSearch,
  setExternalSearch,
  removeOption,
  loading = false,
  required = false,
  filterLogic,
  scrollbar,
  selectCSS,
  targetRef,
}) => {
  const [search, setSearch] = useState("");
  const searchValue = externalSearch !== undefined ? externalSearch : search;
  const setSearchValue =
    setExternalSearch !== undefined ? setExternalSearch : setSearch;

  const [isOpen, setIsOpen] = useState(false);
  const selectRef = useRef(null);
  const [optionList, setOptionList] = useState([]);
  const listRef = useRef(null);
  useOnClickOutside(selectRef, () => {
    setSearchValue("");
    setIsOpen(false);
  });

  const handleOptionClick = (option) => {
    setIsOpen(false);
    setSearchValue("");
    const selectedValue = option.value || "";
    onChange(selectedValue);
  };

  useEffect(() => {
    const handleScroll = () => {
      if (listRef?.current) {
        const { scrollTop, scrollHeight, clientHeight } = listRef.current;

        if (
          totalPage !== undefined &&
          page &&
          page < totalPage &&
          scrollTop + clientHeight >= scrollHeight - 200
        ) {
          setPage(page + 1);
        }
      }
    };

    const list = listRef.current;

    if (list) {
      list.addEventListener("scroll", handleScroll);
    }

    return () => {
      if (list) {
        list.removeEventListener("scroll", handleScroll);
      }
    };
  });

  const handleRemoveOption = (e) => {
    e.stopPropagation();
    removeOption();
  };

  let filteredOptions = options || [];
  if (!externalSearch) {
    filteredOptions = filteredOptions?.filter(
      (option) =>
        (typeof option.label === "string" &&
          option.label.toLowerCase().includes(searchValue.toLowerCase())) ||
        (typeof option.value === "string" &&
          option.value.toLowerCase().includes(searchValue.toLowerCase()))
    );
  }

  return (
    <div ref={targetRef}>
      {label && (
        <p
          className={` pb-2 flex justify-start ${
            leftText && "text-left"
          } font-medium text-[13px] pb-2`}
        >
          {label}{" "}
          {required && <span className="text-red-500 text-xs ml-1">*</span>}
        </p>
      )}
      <div
        ref={selectRef}
        className={`relative flex flex-col justify-start items-start ${
          selectCSS ? selectCSS : "min-w-[13.5rem]"
        } max-w-[100%]`}
      >
        <div
          onClick={() => {
            setIsOpen(!isOpen);
          }}
          className={`w-full flex justify-between items-center py-[7.5px] px-2 bg-white   border-2 border-[#DEDEDE] ${
            disabled
              ? "opacity-30 cursor-not-allowed"
              : "opacity-100 hover:border-2 cursor-pointer  hover:border-[#D4A15E] hover:border-opacity-50 outline-none focus:bg-white focus:border-pot-yellow"
          }`}
        >
          {value ? (
            <span
              className={`block truncate ${width} ${valueFont}  font-gilroy-regular`}
            >
              {value !== "" ? (
                options?.find((option) => option.value === value)?.label ||
                value
              ) : (
                <span className="text-[#AEAEAE] text-sm">{placeholder}</span>
              )}
            </span>
          ) : searchPlaceHolder !== undefined ? (
            <input
              disabled={disabled}
              type="text"
              value={searchValue}
              className={`text-black font-normal w-full outline-none focus:bg-white placeholder-shown:text-pot-grey2  font-gilroy-regular ${width} ${valueFont} leading-5 `}
              onChange={(e) => {
                if (!isOpen) setIsOpen(true);
                setSearchValue(e.target.value);
              }}
              placeholder={searchPlaceHolder}
              onClick={(e) => {
                e.stopPropagation();
                if (!isOpen) setIsOpen(true);
              }}
            />
          ) : (
            <span
              className={`block truncate font-gilroy-regular text-[14px]  w-[90%]`}
            >
              <span className="text-[#AEAEAE] text-sm">{placeholder}</span>
            </span>
          )}

          {value !== "" && removeOption ? (
            <div
              className="scale-105  p-[5.5px] "
              onClick={(e) => {
                if (disabled) {
                  return;
                }
                handleRemoveOption(e);
              }}
            >
              <CrossIcon color="black" />
            </div>
          ) : (
            <motion.div
              animate={{ rotate: isOpen && !disabled ? 180 : 0 }}
              className="w-fit h-fit flex items-center justify-center py-[1px]"
            >
              <HipchatChevronDownIcon color="black" />
            </motion.div>
          )}
        </div>
        {isOpen && !disabled && (
          <div
            ref={listRef}
            className={`w-full bg-white rounded-md shadow-md mt-1 z-50 text-start overflow-y-auto   absolute top-[100%] max-h-[150px]`}
          >
            <ul className={`h-auto overflow-y-auto`}>
              {filterLogic
                ? filterLogic(filteredOptions).map((option) => (
                    <li
                      key={option.value}
                      onClick={() => handleOptionClick(option)}
                      className={`px-4 py-2 cursor-pointer hover:bg-gray-100 font-gilroy-regular text-[14px] leading-5 ${
                        option.value === value ? "bg-gray-100" : ""
                      }`}
                    >
                      {option.label}
                    </li>
                  ))
                : filteredOptions?.map((option) => (
                    <li
                      key={option.value}
                      onClick={() => handleOptionClick(option)}
                      className={`px-4 py-2  cursor-pointer hover:bg-gray-100 font-gilroy-regular ${valueFont} leading-5 ${
                        option.value === value ? "bg-gray-100" : ""
                      }`}
                    >
                      {option.label}
                    </li>
                  ))}
              {loading ? <p>Loading...</p> : null}
              {filteredOptions?.length === 0 && !loading ? (
                <p
                  className={`px-4 py-2 font-gilroy-regular ${valueFont} leading-5 `}
                >
                  No options available
                </p>
              ) : null}
            </ul>
          </div>
        )}
        <ErrorMessage error={hint} />
      </div>
    </div>
  );
};

export default SearchableDropdown;
