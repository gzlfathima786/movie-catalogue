import React from "react";

const ErrorText = ({ text }) => {
  return <p className="text-sm text-pot-maroon">{text}</p>;
};

export default ErrorText;
