import React, { useState, useRef, useEffect } from "react";

const MovieCard = ({ movie }) => {
  const [expandedLanguages, setExpandedLanguages] = useState(false);
  const [expandedCountries, setExpandedCountries] = useState(false);
  const [expandedGenres, setExpandedGenres] = useState(false);

  const languagesRef = useRef(null);
  const countriesRef = useRef(null);
  const genresRef = useRef(null);

  useEffect(() => {
    if (languagesRef.current) {
      const languagesHeight = languagesRef.current.offsetHeight;
      if (languagesHeight > 26) {
        setExpandedLanguages(false);
      }
    }

    if (countriesRef.current) {
      const countriesHeight = countriesRef.current.offsetHeight;
      if (countriesHeight > 26) {
        setExpandedCountries(false);
      }
    }

    if (genresRef.current) {
      const genresHeight = genresRef.current.offsetHeight;
      if (genresHeight > 26) {
        setExpandedGenres(false);
      }
    }
  }, []);

  const toggleExpansion = (section) => {
    switch (section) {
      case "languages":
        setExpandedLanguages(!expandedLanguages);
        break;
      case "countries":
        setExpandedCountries(!expandedCountries);
        break;
      case "genres":
        setExpandedGenres(!expandedGenres);
        break;
      default:
        break;
    }
  };

  return (
    <div className=" flex flex-col gap-2 p-4  hover:border-2 cursor-pointer  hover:border-[#D4A15E]  shadow-xl">
      <div className={`w-full pb-[100%] relative `}>
        {movie.moviemainphotos[0] ? (
          <img
            alt="PX"
            src={movie.moviemainphotos[0]}
            className=" object-stretch absolute w-full h-full"
          />
        ) : (
          <img
            alt="PX"
            src={`https://ipsf.net/wp-content/uploads/2021/12/dummy-image-square.webp`}
            className="object-stretch absolute w-full h-full"
          />
        )}
      </div>

      <h2 className="font-semibold text-xl">{movie.movietitle}</h2>
      {movie.movielanguages.length > 0 ? (
        <p className="text-start" ref={languagesRef}>
          <span className="font-semibold pr-2">Languages:</span>
          {expandedLanguages ? (
            movie.movielanguages.join(", ")
          ) : (
            <>
              {movie.movielanguages.slice(0, 3).join(", ")}
              {movie.movielanguages.length > 3 && " ..."}
            </>
          )}
          {movie.movielanguages.length > 3 && (
            <button
              className="text-blue-500 ml-2"
              onClick={() => toggleExpansion("languages")}
            >
              {expandedLanguages ? "See Less" : "See More"}
            </button>
          )}
        </p>
      ) : (
        <>&nbsp;</>
      )}
      {movie.moviecountries.length > 0 ? (
        <p className="text-start" ref={countriesRef}>
          <span className="font-semibold pr-2">Countries:</span>
          {expandedCountries ? (
            movie.moviecountries.join(", ")
          ) : (
            <>
              {movie.moviecountries.slice(0, 3).join(", ")}
              {movie.moviecountries.length > 3 && " ..."}
            </>
          )}
          {movie.moviecountries.length > 3 && (
            <button
              className="text-blue-500 ml-2"
              onClick={() => toggleExpansion("countries")}
            >
              {expandedCountries ? "See Less" : "See More"}
            </button>
          )}
        </p>
      ) : (
        <>&nbsp;</>
      )}
      {movie.moviegenres.length > 0 ? (
        <p className="text-start" ref={genresRef}>
          <span className="font-semibold pr-2">Genres:</span>
          {expandedGenres ? (
            movie.moviegenres.join(", ")
          ) : (
            <>
              {movie.moviegenres.slice(0, 3).join(", ")}
              {movie.moviegenres.length > 3 && " ..."}
            </>
          )}
          {movie.moviegenres.length > 3 && (
            <button
              className="text-blue-500 ml-2"
              onClick={() => toggleExpansion("genres")}
            >
              {expandedGenres ? "See Less" : "See More"}
            </button>
          )}
        </p>
      ) : (
        <>&nbsp;</>
      )}
    </div>
  );
};

export default MovieCard;
