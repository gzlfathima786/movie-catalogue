import "./App.css";
import React, { useState, useEffect } from "react";
import MovieCard from "./components/MovieCard";
import Filter from "./components/Filter";
import moviesData from "./movies.json";
function App() {
  const [movies, setMovies] = useState([]);
  const [languageOptions, setLanguageOptions] = useState([]);
  const [genreOption, setGenreOption] = useState([]);
  const [countryOption, setCountryOption] = useState([]);
  const [filteredMovies, setFilteredMovies] = useState([]);

  useEffect(() => {
    setMovies(moviesData);
    setFilteredMovies(moviesData);

    const allLanguages = moviesData.reduce((acc, movie) => {
      return [...acc, ...movie.movielanguages];
    }, []);

    const uniqueLanguages = [...new Set(allLanguages)];
    setLanguageOptions(
      uniqueLanguages.map((language) => ({ label: language, value: language }))
    );

    const allGenres = moviesData.reduce((acc, movie) => {
      return [...acc, ...movie.moviegenres];
    }, []);

    const uniqueGenres = [...new Set(allGenres)];
    setGenreOption(
      uniqueGenres.map((genre) => ({ label: genre, value: genre }))
    );

    const allCountries = moviesData.reduce((acc, movie) => {
      return [...acc, ...movie.moviecountries];
    }, []);

    const uniqueCountries = [...new Set(allCountries)];
    setCountryOption(
      uniqueCountries.map((country) => ({ label: country, value: country }))
    );
  }, []);

  return (
    <div className="text-black p-4 flex  flex-col justify-center items-center gap-8">
      <h1 className="w-full text-center m-auto text-4xl font-bold">
        Movies Catalogue
      </h1>
      <Filter
        languageOptions={languageOptions}
        genreOption={genreOption}
        countryOption={countryOption}
        filteredMovies={filteredMovies}
        setFilteredMovies={setFilteredMovies}
        movies={movies}
      />
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-6  p-4 text-center">
        {filteredMovies.map((movie) => (
          <MovieCard key={movie.imdbmovieid} movie={movie} />
        ))}
      </div>
    </div>
  );
}

export default App;
