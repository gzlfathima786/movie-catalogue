/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx,ts,tsx}"],
  theme: {
    extend: {
      screens: {
        sm: "490px",
        md: "744px",
        lg: "1024px",
        xl: "1440px",
        "2xl": "1920px",
      },
    },
  },
  plugins: [],
};
